import React, { Component } from 'react';
import { ScrollView, Text, View } from 'react-native';
import axios from 'axios';
import AlbumDetail from './AlbumDetail';
import Loading from './Loading';
import PhotoComment from './PhotoComment';

class PhotoListComment extends Component {
  constructor(props) {

    super(props);
    this.state = { comments: null};

  }
  componentWillMount() {
    axios.get(`https://api.flickr.com/services/rest/?method=flickr.photos.comments.getList&api_key=${this.props.api_key}&photo_id=${this.props.photo_id}&format=json&nojsoncallback=1`)
      .then(response => this.setState({comments: response.data.comments.comment }))
      .catch(error => {
         console.log('ERROR'); });
      
        }

  renderComments() {
    
    return this.state.comments.map(comment=><PhotoComment key= {comment.id} realName={comment.realname} Comment={comment._content} />);
  }

  render() {
    if (!this.state.comments) { 
			return (
        <View>
          <Text>No Posee Comentarios</Text>
        </View>
				);
    }

    return (
      <View style={{ flex: 1 }}>
        <ScrollView>
          {this.renderComments()}
        </ScrollView>
      </View>
    );
  }
}

export default PhotoListComment;