import React from 'react';
import { Text, View, Image, Linking } from 'react-native';
import Card from './Card';
import CardSection from './CardSection';
import Button from './Button';
import { Actions } from 'react-native-router-flux';

const AlbumDetail = ({ title, albumId, photos, description, api_key, user_id }) => {
  const {
    headerContentStyle,
    infoStyle,
    headerTextStyle,
    imageStyle
  } = styles;

  return (
    <Card>
      <CardSection>
        <View style={headerContentStyle}>
          <Text style={headerTextStyle}>{title}</Text>
        </View>
      </CardSection>
      <CardSection>
        <View style={headerContentStyle}>
          <Text style={infoStyle}>{description}</Text>
          <Text style={infoStyle}>{photos} Fotos</Text>
        </View>
      </CardSection>
      <CardSection>
        <Button onPress={() => Actions.photoList({albumId:albumId, api_key:api_key, user_id:user_id})} icons='folder'>
          Ver Álbum
        </Button>
      </CardSection>
    </Card>
  );
};

const styles = {
  headerContentStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  headerTextStyle: {
    fontSize: 18,
    fontWeight: 'bold',
    color:'#1A444A'
  },
  thumbnailStyle: {
    height: 50,
    width: 50
  },
  thumbnailContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10
  },
  imageStyle: {
    height: 300,
    flex: 1,
    width: null
  },
  infoStyle:{
    fontSize: 12,
    fontStyle: 'italic'
  }
};

export default AlbumDetail;