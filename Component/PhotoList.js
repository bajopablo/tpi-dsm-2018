import React, { Component } from 'react';
import { FlatList, View } from 'react-native';
import { List } from "react-native-elements";
import axios from 'axios';
import PhotoDetail from './PhotoDetail';
import Loading from './Loading'

class PhotoList extends Component {
  constructor(props) {

    super(props);
    this.state = { photos: null,
      api_key:this.props.api_key,
      per_page:3,
      page:1,
      refreshing: false,
      loading: false
    
    };
  }

  componentWillMount() {
    this.makeRemoteRequest();
  }

  makeRemoteRequest = () => {
    const { per_page, page, api_key, user_id } = this.state;

    this.setState({ loading: true });

    axios.get(`https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=${this.props.api_key}&photoset_id=${this.props.albumId}&user_id=${this.props.user_id}&format=json&nojsoncallback=1&per_page=${per_page}&page=${page}`)
      .then(response => this.setState({ photos: page === 1? response.data.photoset.photo: [...this.state.photos, ...response.data.photoset.photo],
        loading: false,
        refreshing: false 
      })).catch(error => {
        this.setState({ loading: false });
      });
  };

  handleRefresh = () => {
    
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  
  };

  handleLoadMore = () => {

    this.setState(
      {
          page: this.state.page + 1
        },
        () => {
          this.makeRemoteRequest();
        }
      );
 
  };

  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
      <Loading></Loading>
    );
  };

  render() {
    if (!this.state.photos) { 
			return (
        <Loading></Loading>  
				);
    }

    return (
      <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
       <FlatList
          data={this.state.photos}
          renderItem={({ item }) => (
            <PhotoDetail id={item.id} key={item.id} api_key={this.state.api_key} title={item.title} imageUrl={`https://farm${item.farm}.staticflickr.com/${item.server}/${item.id}_${item.secret}.jpg`} />
          )}
          keyExtractor={item => item.id}
          ListFooterComponent={this.renderFooter}
          onRefresh={this.handleRefresh}
          refreshing={this.state.refreshing}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={0.5}
        /> 
      
      </List>
  );
  }
}

export default PhotoList;