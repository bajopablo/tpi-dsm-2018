import React, { Component } from 'react';
import { Text, View } from 'react-native';
import CardSection from './CardSection'
import Card from './Card'
const PhotoComment = ({Comment, realName}) =>{
    const {
        headerContentStyle,
        infoStyle,
        headerTextStyleTitle,
        TextStyle,
        imageStyle
      } = styles;

return(
    <CardSection>
        <View style={headerContentStyle}>
            <Text style={headerTextStyleTitle}>{realName}</Text>
            <Text style={TextStyle}>{Comment}</Text>
        </View>
    </CardSection>
    )
}

const styles = {
    headerContentStyle: {
      flexDirection: 'column',
      justifyContent: 'space-around'
    },
    headerTextStyleTitle: {
      fontSize: 14,
      color:'#0040FF',
      fontWeight: 'bold'
    },
    TextStyle: {
      fontSize: 16,
      color:'#6B6D6D',
      //fontWeight: 'bold'
    },
    thumbnailStyle: {
      height: 50,
      width: 50
    },
    thumbnailContainerStyle: {
      justifyContent: 'center',
      alignItems: 'center',
      marginLeft: 10,
      marginRight: 10
    },
    imageStyle: {
      height: 300,
      flex: 1,
      width: null
    }
  };

export default PhotoComment;