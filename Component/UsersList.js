import React, { Component } from 'react';
import { View } from 'react-native';
import UserDetail from './UserDetail';
import Loading from './Loading';

class UsersList extends Component {
  state = { users: null };

  componentWillMount() {
    this.setState({ users: users=[{"name":"Diego","api_key":"5842f435e226e98920ad777d56bfd7dd","user_id":"154843650%40N07"},
                                  {"name":"Claudio","api_key":"6e8a597cb502b7b95dbd46a46e25db8d","user_id":"137290658%40N08"}] });
  }

  renderAlbums() {
    return this.state.users.map(user =>
      <UserDetail key={user.api_key} name={user.name} api_key={user.api_key} user_id={user.user_id} />
    );
  }

  render() {
    
    if (!this.state.users) { 
			return (
        <Loading></Loading>
				);
    }

    return (
      <View style={{ flex: 1 }}>
        
          {this.renderAlbums()}
        
      </View>
    );
  }
}

export default UsersList;