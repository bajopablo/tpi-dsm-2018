import React, { Component } from 'react';
import { ScrollView, Text, View } from 'react-native';
import axios from 'axios';
import AlbumDetail from './AlbumDetail';
import Loading from './Loading';

class AlbumList extends Component {
  state = { photoset: null };

  componentWillMount() {
    axios.get(`https://api.flickr.com/services/rest/?method=flickr.photosets.getList&api_key=${this.props.api_key}&user_id=${this.props.user_id}&format=json&nojsoncallback=1`)
      .then(response => this.setState({ photoset: response.data.photosets.photoset }));
  }

  renderAlbums() {
    return this.state.photoset.map(album =>
      <AlbumDetail key={album.id} title={album.title._content} albumId={album.id} photos={album.photos} description={album.description._content} api_key={this.props.api_key} user_id={this.props.user_id}/>
    );
  }

  render() {
    if (!this.state.photoset) { 
			return (
        <Loading></Loading>
				);
    }

    return (
      <View style={{ flex: 1 }}>
        <ScrollView>
          {this.renderAlbums()}
        </ScrollView>
      </View>
    );
  }
}

export default AlbumList;