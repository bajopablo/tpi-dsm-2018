import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { Icon } from 'react-native-elements'

const Button = ({ onPress, children, icons }) => {
  const { buttonStyle, textStyle, iconStyle } = styles;

  return (
    <TouchableOpacity onPress={onPress} style={buttonStyle}>
      <Icon name={icons} style={iconStyle}/> 
     <Text style={textStyle}>
        {children} 
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  textStyle: {
    alignSelf: 'center',
    color: '#000000',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  },
  buttonStyle: {
    flex: 3,
    backgroundColor: '#007aff',
    borderRadius: 90,
    marginTop :20,
    borderWidth: 2,
    borderColor: '#000000',
    marginLeft: 5,
    marginRight: 5,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  iconStyle: {
    backgroundColor:'#4C4C4C'
  }
};

export default Button;