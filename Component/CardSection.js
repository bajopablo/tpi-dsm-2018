import React from 'react';
import { View } from 'react-native';

const CardSection = (props) => {
  return (
    <View style={styles.containerStyle}>
      {props.children}
    </View>
  );
};

const styles = {
  containerStyle: {
    borderBottomWidth: 1,
    //borderRadius: 5,
    padding: 5,
    backgroundColor: '#E6F6F8',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#1A444A',
    position: 'relative'
  }
};

export default CardSection;