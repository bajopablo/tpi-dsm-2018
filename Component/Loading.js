import React from 'react';
import { View, ActivityIndicator} from 'react-native';

const Loading = () => {
  return (
    <View style={{ flex: 2 }}>
        <ActivityIndicator animating size="large" color="#007aff"/>
    </View>
  );
};

export default Loading;