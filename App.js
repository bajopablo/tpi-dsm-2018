import React from 'react';
import { AppRegistry, View } from 'react-native';
import Header from './Component/Header';
import AlbumList from './Component/AlbumList';
import PhotoList from './Component/PhotoList';
import UsersList from './Component/UsersList';
import { Router, Scene } from 'react-native-router-flux';

export default class App extends React.Component {
  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene key="usersList" component={UsersList} title="Usuarios" initial={true} />
          <Scene key="albumList" component={AlbumList} title="Álbumes" />
          <Scene key="photoList" component={PhotoList} title="Fotos" />
        </Scene>
      </Router>
    )
}
}