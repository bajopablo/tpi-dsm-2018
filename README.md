Trabajo Practico Integrador de la materia Desarrollo de Software Multipantalla de la Universidad Tecnológica Nacional Facultad Regional Córdoba del año 2018

## Table of Contents

* [Integrantes] (#integrantes)
* [Pruebas] (#pruebas)

## Integrantes

* Bajo, Pablo Leg: 51084  Email: [haploide666@gmail.com] (mailto:haploide666@gmail.com)
* Quezada, Jose Leg: 55251 Email: [jwqm.24@gmail.com] (mailto:jwqm.24@gmail.com)
* Lopez, Federico Leg: 55025 Email: [federico.lopez1991@gmail.com] (mailto:federico.lopez1991@gmail.com)
* Moreno, Diego Leg: 45614 Email: [diego149@hotmail.com] (mailto:diego149@hotmail.com)

## Pruebas

Las pruebas fueron realizadas utilizando celulares android y la app Expo.